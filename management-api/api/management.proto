syntax = "proto3";

package nlx.management;

option go_package = "go.nlx.io/nlx/management-api/api";

import "google/api/annotations.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

service Management {
  rpc SynchronizeOrders(google.protobuf.Empty) returns (SynchronizeOrdersResponse) {
    option (google.api.http) = {post: "/api/v1/orders/synchronize"};
  }
  rpc IsFinanceEnabled (google.protobuf.Empty) returns (IsFinanceEnabledResponse) {
    option (google.api.http) = {get: "/api/v1/finance/enabled"};
  }
  rpc DownloadFinanceExport (google.protobuf.Empty) returns (DownloadFinanceExportResponse) {
    option (google.api.http) = {get: "/api/v1/finance/export"};
  }
  rpc ListServices (ListServicesRequest) returns (ListServicesResponse) {
    option (google.api.http) = {get: "/api/v1/services"};
  }
  rpc GetService (GetServiceRequest) returns (GetServiceResponse) {
    option (google.api.http) = {get: "/api/v1/services/{name}"};
  }
  rpc CreateService (CreateServiceRequest) returns (CreateServiceResponse) {
    option (google.api.http) = {post: "/api/v1/services" body: "*"};
  }
  rpc UpdateService (UpdateServiceRequest) returns (UpdateServiceResponse) {
    option (google.api.http) = {put: "/api/v1/services/{name}" body: "*"};
  }
  rpc DeleteService (DeleteServiceRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {delete: "/api/v1/services/{name}"};
  }
  rpc GetStatisticsOfServices (GetStatisticsOfServicesRequest) returns (GetStatisticsOfServicesResponse) {
    option (google.api.http) = {get: "/api/v1/statistics/services"};
  }
  rpc ListInways (ListInwaysRequest) returns (ListInwaysResponse) {
    option (google.api.http) = {get: "/api/v1/inways"};
  }
  rpc GetInway (GetInwayRequest) returns (Inway) {
    option (google.api.http) = {get: "/api/v1/inways/{name}"};
  }
  rpc CreateInway (Inway) returns (Inway) {
    option (google.api.http) = {post: "/api/v1/inways" body: "*"};
  }
  rpc UpdateInway (UpdateInwayRequest) returns (Inway) {
    option (google.api.http) = {put: "/api/v1/inways/{name}" body: "inway"};
  }
  rpc DeleteInway (DeleteInwayRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {delete: "/api/v1/inways/{name}"};
  }
  rpc ListIncomingAccessRequest (ListIncomingAccessRequestsRequests) returns (ListIncomingAccessRequestsResponse) {
    option (google.api.http) = {get: "/api/v1/access-requests/incoming/services/{serviceName}"};
  }
  rpc ApproveIncomingAccessRequest (ApproveIncomingAccessRequestRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {post: "/api/v1/access-requests/incoming/services/{serviceName}/{accessRequestID}/approve"};
  }
  rpc RejectIncomingAccessRequest (RejectIncomingAccessRequestRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {post: "/api/v1/access-requests/incoming/services/{serviceName}/{accessRequestID}/reject"};
  }
  rpc CreateAccessRequest (CreateAccessRequestRequest) returns (OutgoingAccessRequest) {
    option (google.api.http) = {post: "/api/v1/access-requests" body: "*"};
  }
  rpc SendAccessRequest (SendAccessRequestRequest) returns (OutgoingAccessRequest) {
    option (google.api.http) = {post: "/api/v1/access-requests/outgoing/organizations/{organizationName}/services/{serviceName}/{accessRequestID}/send"};
  }
  rpc GetSettings (google.protobuf.Empty) returns (Settings) {
    option (google.api.http) = {get: "/api/v1/settings"};
  }
  rpc UpdateSettings(UpdateSettingsRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {put: "/api/v1/settings", body: "*"};
  }
  rpc ListAccessGrantsForService(ListAccessGrantsForServiceRequest) returns(ListAccessGrantsForServiceResponse) {
    option (google.api.http) = {get: "/api/v1/access-grants/services/{serviceName}"};
  }
  rpc RevokeAccessGrant(RevokeAccessGrantRequest) returns (AccessGrant) {
    option (google.api.http) = {post: "/api/v1/access-grants/service/{serviceName}/organizations/{organizationName}/{accessGrantID}/revoke"};
  }
  rpc ListAuditLogs (google.protobuf.Empty) returns (ListAuditLogsResponse) {
    option (google.api.http) = {get: "/api/v1/audit-logs"};
  }
  rpc RetrieveClaimForOrder (RetrieveClaimForOrderRequest) returns (RetrieveClaimForOrderResponse) {
    option (google.api.http) = {get: "/api/v1/retrieve-claim"};
  }
  rpc CreateOutgoingOrder (CreateOutgoingOrderRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {post: "/api/v1/orders/outgoing" body: "*"};
  }
  rpc RevokeOutgoingOrder (RevokeOutgoingOrderRequest) returns (google.protobuf.Empty){
    option (google.api.http) = {put: "/api/v1/orders/outgoing/{delegatee}/{reference}/revoke"}; 
  }
  rpc ListOutgoingOrders (google.protobuf.Empty) returns (ListOutgoingOrdersResponse) {
    option (google.api.http) = {get: "/api/v1/orders/outgoing"};
  }
  rpc ListIncomingOrders (google.protobuf.Empty) returns (ListIncomingOrdersResponse) {
    option (google.api.http) = {get: "/api/v1/orders/incoming"};
  }
}

message SynchronizeOrdersResponse {
  repeated IncomingOrder orders = 1;
}

message IsFinanceEnabledResponse {
  bool enabled = 1;
}

message DownloadFinanceExportResponse {
  bytes data = 1;
}

message Service {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  AuthorizationSettings authorizationSettings = 8;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;

  message AuthorizationSettings {
    string mode = 1;
    repeated Authorization authorizations = 2;

    message Authorization  {
      string organizationName = 1;
      string publicKeyHash = 2;
    }
  }
}

message GetServiceResponse {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;
}

message CreateServiceRequest {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;
}

message CreateServiceResponse {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;
}

message UpdateServiceRequest {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;
}

message UpdateServiceResponse {
  string name = 1;
  string endpointURL = 2;
  string documentationURL = 3;
  string apiSpecificationURL = 4;
  bool internal = 5;
  string techSupportContact = 6;
  string publicSupportContact = 7;
  repeated string inways = 9;
  int32 oneTimeCosts = 10;
  int32 monthlyCosts = 11;
  int32 requestCosts = 12;
}

message Inway {
  message Service {
    string name = 1;
  }

  string name = 1;
  string version = 2;
  string hostname = 3;
  string selfAddress = 4;
  repeated Service services = 5;
  string ipAddress = 6;
}

message GetStatisticsOfServicesRequest {}

message GetStatisticsOfServicesResponse {
  repeated ServiceStatistics services = 1;
}

message ServiceStatistics {
  string name = 1;
  uint32 incomingAccessRequestCount = 2;
}

message ListServicesRequest {
  string inwayName = 1;
}

message ListServicesResponse {
  repeated Service services = 1;

  message Service {
    string name = 1;
    string endpointURL = 2;
    string documentationURL = 3;
    string apiSpecificationURL = 4;
    bool internal = 5;
    string techSupportContact = 6;
    string publicSupportContact = 7;
    AuthorizationSettings authorizationSettings = 8;
    repeated string inways = 9;
    uint32 incomingAccessRequestCount = 10;
    int32 oneTimeCosts = 11;
    int32 monthlyCosts = 12;
    int32 requestCosts = 13;

    message AuthorizationSettings {
      string mode = 1;
      repeated Authorization authorizations = 2;

      message Authorization  {
        string organizationName = 1;
        string publicKeyHash = 2;
        string publicKeyPEM = 3;
      }
    }
  }
}

message GetServiceRequest {
  string name = 1;
}

message DeleteServiceRequest {
  string name = 1;
}

message ListInwaysRequest {}

message ListInwaysResponse {
  repeated Inway inways = 1;
}

message GetInwayRequest {
  string name = 1;
}

message UpdateInwayRequest {
  string name = 1;
  Inway inway = 2;
}

message DeleteInwayRequest {
  string name = 1;
}

enum AccessRequestState {
  UNSPECIFIED = 0;
  FAILED = 1;
  CREATED = 2;
  RECEIVED = 3;
  APPROVED = 4;
  REJECTED = 5;
}

enum ErrorCode {
  INTERNAL = 0;
  NO_INWAY_SELECTED = 1;
}

message ErrorDetails {
  ErrorCode code = 1;
  string cause = 2;
  repeated string stackTrace = 3;
}

message OutgoingAccessRequest {
  uint64 id = 1;
  string organizationName = 2;
  string serviceName = 3;
  AccessRequestState state = 4;
  google.protobuf.Timestamp createdAt = 5;
  google.protobuf.Timestamp updatedAt = 6;
  ErrorDetails errorDetails = 7;
}

message IncomingAccessRequest {
  uint64 id = 1;
  string organizationName = 2;
  string serviceName = 3;
  AccessRequestState state = 4;
  google.protobuf.Timestamp createdAt = 5;
  google.protobuf.Timestamp updatedAt = 6;
}

message ListIncomingAccessRequestsRequests {
  string serviceName = 1;
}

message ListIncomingAccessRequestsResponse {
  repeated IncomingAccessRequest accessRequests = 1;
}

message ApproveIncomingAccessRequestRequest {
  string serviceName = 1;
  uint64 accessRequestID = 2;
}

message RejectIncomingAccessRequestRequest {
  string serviceName = 1;
  uint64 accessRequestID = 2;
}

message ListOutgoingAccessRequestsRequest {
  string organizationName = 1;
  string serviceName = 2;
}

message ListOutgoingAccessRequestsResponse {
  repeated OutgoingAccessRequest accessRequests = 1;
}

message CreateAccessRequestRequest {
  string organizationName = 1;
  string serviceName = 2;
}

message SendAccessRequestRequest {
  string organizationName = 1;
  string serviceName = 2;
  uint64 accessRequestID = 3;
}

message ListAccessGrantsForServiceRequest {
  string serviceName = 1;
}

message ListAccessGrantsForServiceResponse {
  repeated AccessGrant accessGrants = 1;
}

message RevokeAccessGrantRequest {
  string organizationName = 1;
  string serviceName = 2;
  uint64 accessGrantID = 3;
}

message AccessGrant {
  uint64 id = 1;
  string organizationName = 2;
  string serviceName = 3;
  string publicKeyFingerprint = 4;
  google.protobuf.Timestamp createdAt = 5;
  google.protobuf.Timestamp revokedAt = 6;
  uint64 accessRequestId = 7;
}

message AccessProof {
  uint64 id = 1;
  string organizationName = 2;
  string serviceName = 3;
  google.protobuf.Timestamp createdAt = 4;
  google.protobuf.Timestamp revokedAt = 5;
  uint64 accessRequestId = 6;
}

service Directory {
  rpc ListServices (google.protobuf.Empty) returns (DirectoryListServicesResponse) {
    option (google.api.http) = {get: "/api/v1/directory/services"};
  }
  rpc GetOrganizationService (GetOrganizationServiceRequest) returns (DirectoryService) {
    option (google.api.http) = {get: "/api/v1/directory/organizations/{organizationName}/services/{serviceName}"};
  }
  rpc RequestAccessToService (RequestAccessToServiceRequest) returns (OutgoingAccessRequest) {
    option (google.api.http) = {post: "/api/v1/directory/organizations/{organizationName}/services/{serviceName}/access-requests"};
  }
}

message DirectoryListServicesResponse {
  repeated DirectoryService services = 1;
}

message GetOrganizationServiceRequest {
  string organizationName = 1;
  string serviceName = 2;
}

message DirectoryAccessRequest {
  string id = 1;
  AccessRequestState state = 2;
  google.protobuf.Timestamp createdAt = 3;
  google.protobuf.Timestamp updatedAt = 4;
}

message DirectoryService {
  enum State {
    unknown = 0;
    up = 1;
    down = 2;
    degraded = 3;
  }

  string serviceName = 1;
  string organizationName = 2;
  string apiSpecificationType = 3;
  string documentationURL = 4;
  string publicSupportContact = 5;
  State state = 6;
  OutgoingAccessRequest latestAccessRequest = 7;
  AccessProof latestAccessProof = 8;
  int32 oneTimeCosts = 9;
  int32 monthlyCosts = 10;
  int32 requestCosts = 11;
}

message RequestAccessToServiceRequest {
  string organizationName = 1;
  string serviceName = 2;
}

message Settings {
  string organizationInway = 1;
}

message UpdateSettingsRequest{
  string organizationInway = 1;
}

message AuditLogRecord {
  enum ActionType {
    loginSuccess = 0;
    loginFail = 1;
    logout = 2;
    incomingAccessRequestAccept = 3;
    incomingAccessRequestReject = 4;
    accessGrantRevoke = 5;
    outgoingAccessRequestCreate = 6;
    outgoingAccessRequestFail = 7;
    serviceCreate = 8;
    serviceUpdate = 9;
    serviceDelete = 10;
    organizationSettingsUpdate = 11;
    orderCreate = 13;
    orderOutgoingRevoke = 14;
    orderIncomingRevoke = 15;
  }

  message Service {
    string organization = 1;
    string service = 2;
  }

  uint64 id = 1;
  ActionType action = 2;
  string operatingSystem = 3;
  string browser = 4;
  string client = 5;
  string user = 6;
  string delegatee = 7;
  repeated Service services = 8;
  google.protobuf.Timestamp createdAt = 9;
  AuditLogRecordMetadata data = 10;
}

message AuditLogRecordMetadata {
  string delegatee = 1;
  string delegator = 2;
  string reference = 3;
}

message ListAuditLogsResponse {
  repeated AuditLogRecord auditLogs = 1;
}

message RetrieveClaimForOrderRequest {
  string orderReference = 1;
  string orderOrganizationName = 2;
}

message RetrieveClaimForOrderResponse {
  string claim = 1;
}

message CreateOutgoingOrderRequest {
  string reference = 1;
  string description = 2;
  string publicKeyPEM = 3;
  string delegatee = 4;
  google.protobuf.Timestamp validFrom = 5;
  google.protobuf.Timestamp validUntil = 6;
  repeated OrderService services = 7;
}

message RevokeOutgoingOrderRequest{
  string delegatee = 1;
  string reference = 2; 
}

message OrderService {
  string organization = 1;
  string service = 2;
}

message OutgoingOrder {
  string reference = 1;
  string description = 2;
  string delegatee = 4;
  google.protobuf.Timestamp validFrom = 5;
  google.protobuf.Timestamp validUntil = 6;
  repeated OrderService services = 7;
  google.protobuf.Timestamp revokedAt = 8;
}

message ListOutgoingOrdersResponse {
  repeated OutgoingOrder orders = 1;
}

message IncomingOrder {
  string reference = 1;
  string description = 2;
  string delegator = 3;
  google.protobuf.Timestamp validFrom = 4;
  google.protobuf.Timestamp validUntil = 5;
  repeated OrderService services = 6;
  google.protobuf.Timestamp revokedAt = 7;
}

message ListIncomingOrdersResponse {
  repeated IncomingOrder orders = 1;
}
