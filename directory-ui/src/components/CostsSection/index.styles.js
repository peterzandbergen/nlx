// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//

import styled from 'styled-components'
import Table from '../DirectoryTable/Table'

export const TdPrice = styled(Table.Td)`
  text-align: right;
`
