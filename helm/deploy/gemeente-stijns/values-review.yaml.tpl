###########
## Chart ##
###########

################
## Sub-charts ##
################
nlx-management:
  config:
    oidc:
      clientSecret: grGSl5W5HcKRETBr3OhmU6Tm
      discoveryURL: https://dex-gemeente-stijns-{{DOMAIN_SUFFIX}}
      redirectURL: https://nlx-management-gemeente-stijns-{{DOMAIN_SUFFIX}}/oidc/callback
      sessionSignKey: 0Xn2DBfb4L4hwN3XosbwoKZalLBU68UU
  ingress:
    hosts:
      - nlx-management-gemeente-stijns-{{DOMAIN_SUFFIX}}

dex:
  config:
    issuer: https://dex-gemeente-stijns-{{DOMAIN_SUFFIX}}
    staticClients:
      - id: nlx-management
        name: NLX Management
        secret: grGSl5W5HcKRETBr3OhmU6Tm
        redirectURIs:
          - https://nlx-management-gemeente-stijns-{{DOMAIN_SUFFIX}}/oidc/callback
  ingress:
    hosts:
      - dex-gemeente-stijns-{{DOMAIN_SUFFIX}}

video-player-ui:
  organizationName: "Gemeente Stijns"
  outwayServiceBaseUrl: http://gemeente-stijns-nlx-outway/RvRD/voorbeeld-video-stream
  ingress:
    enabled: true
    hosts:
      - nlx-video-player-ui-gemeente-stijns-{{DOMAIN_SUFFIX}}

nginx-websockets-proxy:
  organizationName: "Gemeente Stijns"
  outwayServiceBaseUrl: http://gemeente-stijns-nlx-outway/RvRD/voorbeeld-websockets
  ingress:
    enabled: true
    hosts:
        # abbreviated name, because https://gitlab.com/commonground/nlx/nlx/-/blob/master/technical-docs/notes.md#1215-rename-current-organizations
      - nlx-nginx-ws-p-gemeente-stijns-{{DOMAIN_SUFFIX}}

websockets-chat-ui:
  organizationName: "Gemeente Stijns"
  websocketsProxyBaseUrl: wss://nlx-nginx-ws-p-gemeente-stijns-{{DOMAIN_SUFFIX}}
  ingress:
    enabled: true
    hosts:
        # abbreviated name, because https://gitlab.com/commonground/nlx/nlx/-/blob/master/technical-docs/notes.md#1215-rename-current-organizations
      - nlx-ws-chat-ui-gemeente-stijns-{{DOMAIN_SUFFIX}}
