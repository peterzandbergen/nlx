---
id: ip-addresses
title: IP-addresses
---

## Central NLX components

The central NLX components (directory) are located at the following IP addresses:

### Demo

- directory-registration-api (`20.86.244.209:443`)
- directory-inspection-api (`20.86.243.85:443`)

### Pre-production

- directory-registration-api (`20.86.243.126:443`)
- directory-inspection-api (`20.86.244.123:443`)

### Production

- directory-registration-api (`20.86.244.12:443`)
- directory-inspection-api (`20.76.229.234:443`)

## Your NLX components

You need to open the following ports in your firewall for your NLX components:

- inway (`8443`)
- management-api (`8444`)
(unless you defined other ports)
